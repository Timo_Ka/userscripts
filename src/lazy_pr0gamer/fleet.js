import {GM_getValue, GM_setValue, pad} from "./general";
import {calcexpopoints} from "./fleet_custom";

export function set_arrival_time() {

    if (window.location.href.includes("page=fleetStep1")) {
        let xf = document.getElementsByClassName("table519")
        xf[xf.length - 1].getElementsByTagName("input")[0].addEventListener("click", fleet_set_target);
    }
}

function fleet_set_target() {
    GM_setValue("fleet_arrival", dataFlyTime);
    const t_koords = document.getElementById("galaxy").value + ":" + document.getElementById("system").value + ":" + document.getElementById("planet").value

    GM_setValue("fleet_t_coords", [document.getElementById("planet").value, t_koords]);
}

function get_current_ships() {
    const newfleet = {}
    const shipids = [212, 202, 203, 210, 209, 208, 204, 205, 206, 207, 215, 211, 213, 214]
    for (let ship of shipids) {
        let shipamt = document.getElementById("ship" + ship + "_value")
        if (shipamt) {
            newfleet[ship] = parseInt(shipamt.innerText.replaceAll(".", ""))
        } else {
            newfleet[ship] = 0
        }
    }
    return newfleet


}

function get_selected_ships() {
    let ships = {}
    const shipids = [212, 202, 203, 210, 209, 208, 204, 205, 206, 207, 215, 211, 213, 214]
    for (let ship of shipids) {
        let shipamt = document.getElementById("ship" + ship + "_input")
        if (shipamt) {
            ships[ship] = parseInt(shipamt.value.replaceAll(".", ""))
        } else {
            ships[ship] = 0
        }
    }
    return ships

}


function get_expos_on_koord(koord) {

    const expos = GM_getValue("fleet_expos", {})
    if (!(koord in expos)) {
        return 0;
    }
    let nary = []
    const ctime = Date.now()
    for (let time of expos[koord]) {
        if (ctime - time < 86400000) {
            nary.push(time)
        }

    }
    expos[koord] = nary
    GM_setValue("fleet_expos", expos);
    return nary.length

}

function fleet_Expo_add() {
    const expos = GM_getValue("fleet_expos", {})
    const target = GM_getValue("fleet_t_coords", ["-1", "-1"])[1]
    if (!(target in expos)) {
        expos[target] = [];
    }
    expos[target].push(Date.now())
    GM_setValue("fleet_expos", expos);
}

export function show_arrival() {
    if (!window.location.href.includes("page=fleetStep2")) {
        return;
    }
    let tbl = document.getElementsByClassName("table519")[0]
    const target = GM_getValue("fleet_t_coords", ["-1", "-1"])
    tbl.getElementsByTagName("table")[1].getElementsByTagName("td")[11].innerHTML += '<br> <a href="#" id="save_Res">Ausgewählte Rohstoffe Laden</a>'
    document.getElementById("save_Res").addEventListener("click", fleet_insert_res);
    if (target[0] == "16") {
        let expoamt = get_expos_on_koord(target[1])
        tbl.getElementsByTagName("table")[1].insertRow(7)
        tbl.getElementsByTagName("table")[1].getElementsByTagName("tr")[7].innerHTML = '<td class="transparent" colspan="3">Expos:' + expoamt + '</td>'
        document.getElementsByClassName("table519")[0].getElementsByTagName("input")[document.getElementsByClassName("table519")[0].getElementsByTagName("input").length - 1].addEventListener("click", fleet_Expo_add);
    }


}

function fleet_insert_res() {
    let f = GM_getValue("fleet_trans_res", [0, 0, 0]);
    document.getElementsByName("metal")[0].value = f[0]
    document.getElementsByName("crystal")[0].value = f[1]
    document.getElementsByName("deuterium")[0].value = f[2]
    calculateTransportCapacity();
}



export function show_exp_value() {
    if (!window.location.href.includes("pr0game.com/game.php?page=fleetTable")) {
        return
    }
    if (document.getElementsByClassName("table519")[0].getElementsByTagName("tr")[0].innerText.includes(":") === false) {
        return;
    }

    let fleets = document.getElementsByClassName("table519")[0]
    let fpoints = 0
    let ships = get_selected_ships()
    for (let s in ships) {
        fpoints += exp_values[s] * ships[s]
    }
    fleets.getElementsByTagName("td")[4].innerText = fpoints + " / " + GM_getValue("exp_max", -1)
}


function get_best_ship_type() {
    let bestscore = 0;
    let bestid = -1

    const prio = [205, 206, 207, 215, 211, 213]
    const ships = get_current_ships()
    for (let s of prio) {
        if (ships[s] !== 0) {
            bestid = s
            bestscore = exp_values[s]
        }
    }

    return [bestid, bestscore]
}


function fleet_expo() {
    if (!window.location.href.includes("pr0game.com/game.php?page=fleetTable")) {
        return
    }
    noShips();
    let beststuff = get_best_ship_type()
    let gts_needed = Math.ceil((GM_getValue("exp_max", -1) - beststuff[1]) / 60)
    let gtamt = document.getElementById("ship203_value")
    if (gtamt) {
        gtamt = parseInt(gtamt.innerText.replaceAll(".", ""))
    } else {
        gtamt = 0
    }
    let spy = document.getElementById("ship210_input")
    if (spy) {
        spy.value = 1
    }
    if (gtamt > 0) {
        document.getElementById("ship203_input").value = Math.min(gts_needed, gtamt)
    }
    if (beststuff[0] !== -1) {
        document.getElementById("ship" + beststuff[0] + "_input").value = "1"
    }
    let expocount = calcexpopoints(get_selected_ships())
    if (expocount < GM_getValue("exp_max", -1)) {
        alert("not at expo cap!")
    }
    show_exp_value();
}


function custom_expo() {
    if (!window.location.href.includes("pr0game.com/game.php?page=fleetTable")) {
        return
    }
    noShips();
    const custom_fleet = GM_getValue("custom_fleets", {})[this.getAttribute("cfleet")]
    let warn = false;
    for (let stype in custom_fleet) {
        let obj = document.getElementById(`ship${stype}_input`)
        if (obj) {
            obj.value = custom_fleet[stype]

        } else {
            console.log(custom_fleet[stype],stype,custom_fleet[stype] === "0" || custom_fleet[stype] === "")
            if (custom_fleet[stype] === "0" || custom_fleet[stype] === "") {

            }else {
                warn = true;
            }
        }
    }
    show_exp_value();
    if (warn) {
        alert("not all ships could be selected!")
    }
}

function gt_expo() {
    if (!window.location.href.includes("pr0game.com/game.php?page=fleetTable")) {
        return
    }
    noShips();
    let gts_needed = Math.ceil((GM_getValue("exp_max", -1)) / 60)
    let gtamt = document.getElementById("ship203_value")
    if (gtamt) {
        gtamt = parseInt(gtamt.innerText.replaceAll(".", ""))
    } else {
        gtamt = 0
    }
    let spy = document.getElementById("ship210_input")
    if (spy) {
        spy.value = 1
    }
    if (gtamt > 0) {
        document.getElementById("ship203_input").value = Math.min(gts_needed, gtamt)
    }

    let expocount = calcexpopoints(get_selected_ships())
    if (expocount < GM_getValue("exp_max", -1)) {
        alert("not at expo cap!")
    }

    show_exp_value();
}


function fleet_showcustom() {
    let cfleet = GM_getValue("custom_fleets", {})
    let x = ""
    for (let k in cfleet) {
        x += '   <a cfleet="' + k + '" class="cfleet" href="javascript:;">' + k + '</a>'

    }
    return x;
}

export function add_expo_buttons() {
    if (!window.location.href.includes("pr0game.com/game.php?page=fleetTable")) {
        return
    }
    if (document.getElementsByClassName("table519")[0].getElementsByTagName("tr")[0].innerText.includes(":") === false) {
        return;
    }

    let fleets = document.getElementsByClassName("table519")[0]

    let f_rows = fleets.getElementsByTagName("tr")

    let ex = fleet_showcustom()
    let row = document.createElement("tr");


    row.innerHTML += `<td colspan="5"><center>
<a id="gt_expo" href="javascript:;">GT_expo</a>
<a id="fleet_expo" href="javascript:;">Fleet_expo</a>
` + ex + `</center></td> `
    fleets.childNodes[1].insertBefore(row, f_rows[f_rows.length - 1]);
    document.getElementById("gt_expo").addEventListener("click", gt_expo);
    document.getElementById("fleet_expo").addEventListener("click", fleet_expo);
    for (let node of document.querySelectorAll(".cfleet")) {

        node.addEventListener("click", custom_expo);
    }
    let sdiv = `<center id="res_sender">Met:<input type="number" value="${document.getElementById("current_metal").innerText.replace(/\./g, "")}"> Krist:<input type="number" value="${document.getElementById("current_crystal").innerText.replace(/\./g, "")}"> Deut:<input type="number" value="${document.getElementById("current_deuterium").innerText.replace(/\./g, "")}"> <br><button id="gt_send" dt="ship203_input">gt (42)</button> <button id="kt_send" dt="ship202_input">kt (132)</button><br></center>`

    let x = document.createElement("div");
    x.innerHTML = sdiv
    document.getElementsByTagName("content")[0].insertBefore(x, document.getElementsByTagName("content")[0].childNodes[6])
    for (let ipt of x.getElementsByTagName("input")) {

        ipt.addEventListener('change', set_tranport_buttons);
    }
    for (let ipt of x.getElementsByTagName("button")) {

        ipt.addEventListener('click', set_transport_amt);
    }
    set_tranport_buttons();
}

function set_tranport_buttons() {
    let ttl = 0
    for (let ipt of document.getElementById("res_sender").getElementsByTagName("input")) {
        ttl += parseInt(ipt.value)
    }

    let gts = Math.floor(ttl / 25000)
    let kts = Math.floor(ttl / 5000)
    if (ttl / 25000 - gts > 0.8) {
        gts += 2
    } else {
        gts += 1
    }
    if (ttl / 5000 - kts > 0.6) {
        kts += 2
    } else {
        kts += 1
    }
    document.getElementById("gt_send").innerText = "gt select (" + gts + ")"
    document.getElementById("kt_send").innerText = "kt select (" + kts + ")"
    document.getElementById("gt_send").setAttribute("data", gts)
    document.getElementById("kt_send").setAttribute("data", kts)
    document.getElementsByClassName("table519")[0].querySelector('input[type="submit"]')?.addEventListener('click', save_trans_res);
}

function set_transport_amt() {
    let amt = this.getAttribute("data")
    let stype = this.getAttribute("dt")
    noShips()
    document.getElementById(stype).value = amt;

}

function save_trans_res() {
    let inpts = document.getElementById("res_sender").getElementsByTagName("input")
    GM_setValue("fleet_trans_res", [inpts[0].value, inpts[1].value, inpts[2].value]);
}


export function add_exp_eventlisteners() {
    if (!window.location.href.includes("pr0game.com/game.php?page=fleetTable")) {
        return
    }

    for (let sid in exp_values) {
        let shipinpt = document.getElementById("ship" + sid + "_input")
        if (shipinpt) {
            shipinpt.addEventListener("change", () => setTimeout(show_exp_value, 100));
        }

    }


}


export function set_max_exp_score() {
    let stats = document.getElementById("stats")
    if (!stats) {
        return
    }
    if (stats.getElementsByTagName("select")[1].value != "1") {
        return
    }
    if (document.getElementsByClassName("tooltip")[0].innerText != "1") {
        return
    }

    let bestscore = parseInt(document.getElementsByClassName("table519")[1].getElementsByTagName("td")[4].innerText.replace(/\./g, ""))
    let exposcore = 2400
    if (bestscore > 100000) {
        exposcore = 6000
    }
    if (bestscore > 1000000) {
        exposcore = 9000
    }
    if (bestscore > 5000000) {
        exposcore = 12000
    }
    if (bestscore > 25000000) {
        exposcore = 15000
    }
    if (bestscore > 50000000) {
        exposcore = 18000
    }

    if (bestscore > 75000000) {
        exposcore = 21000
    }
    if (bestscore > 100000000) {
        exposcore = 25000
    }

    GM_setValue("exp_max", exposcore)
}


const exp_values = {
    "202": 20,
    "203": 60,
    "204": 20,
    "205": 50,
    "206": 135,
    "207": 300,
    "208": 150,
    "209": 80,
    "210": 5,
    "211": 375,
    "212": 0,
    "213": 550,
    "214": 45000,
    "215": 350
}

const battleships = ["Leichter Jäger", "Schwerer Jäger", "Kreuzer", "Schlachtschiff", "Schlachtkreuzer", "Bomber", "Zerstörer", "Light Fighter", "Heavy Fighter", "Cruiser", "Battleship", "Battle Cruiser", "Planet Bomber", "Star Fighter"]
