import {get_current_planet_id, get_sortet_planet_list, GM_getValue, GM_setValue} from "./general";
import {get_current_values} from "./resources";

export function commander_building_reader() {
    if (!window.location.href.includes("page=buildings")) {
        return
    }
    let defbuildings = document.getElementsByClassName("buildn")
    const amtregex = /\d+/g
    const def_obj = {}
    for (let def of defbuildings) {
        let text = def.innerText.replaceAll(".", "");
        let id = def.firstElementChild.getAttribute('onclick').match(amtregex)
        let match = text.match(amtregex)
        if (match && match.length === 1 && id.length === 1) {
            def_obj[id[0]] = parseInt(match[0])
        }

    }
    const commander_def = GM_getValue("commander_building", {})
    commander_def[get_current_planet_id()] = def_obj
    GM_setValue("commander_building", commander_def)
}

export function commander_def_reader() {
    if (!window.location.href.includes("mode=defense")) {
        return
    }
    let defbuildings = document.getElementsByTagName("form")
    const amtregex = /\d+/g
    const def_obj = {}
    for (let def of defbuildings) {
        let text = def.firstElementChild.innerText.replaceAll(".", "");
        let match = text.match(amtregex)
        if (match && match.length === 1 && def.id) {
            def_obj[def.id] = parseInt(match[0])
        }

    }
    const commander_def = GM_getValue("commander_def", {})
    commander_def[get_current_planet_id()] = def_obj
    GM_setValue("commander_def", commander_def)
}

export function save_planet_fleet() {
    if (!window.location.href.includes("page=fleetTable")) {
        return
    }
    const fleet_overview = GM_getValue("commander_fleet", {})
    let coords = get_current_planet_id()
    const newfleet = {}
    const shipids = [212, 202, 203, 210, 209, 208, 204, 205, 206, 207, 215, 211, 213, 214]
for(let ship of shipids){
    let shipamt=document.getElementById("ship"+ship+"_value")
    if(shipamt){
        newfleet[ship]=parseInt(shipamt.innerText.replaceAll(".",""))
    }else{
        newfleet[ship]=0
    }
    }
    fleet_overview[coords] = newfleet
    GM_setValue("commander_fleet", fleet_overview);
}

export function get_imperiom_stuff() {

    if (!window.location.href.includes("page=imperium")) {
        return
    }
    const now = Date.now()
    const tblrows = document.getElementsByTagName("content")[0].getElementsByTagName("tr")

    const pids = ["abc", "def"]


    let planets = document.getElementById("planetSelector")
    for (let p of planets.children) {
        let pname = p.innerText.split("[")[1]
        if (p.innerText.includes("(")) {
            pname = pname + "M"
        }
        pids.push(pname)
    }

    for (let i in tblrows) {
        let spt = String(tblrows[i].innerText).split("\t")
        if (spt.length === pids.length) {
            if (spt[1] === "0") {
                tblrows[i].style.display = "None"
            }
        }

    }
    // res and production
    const res = GM_getValue("res", {})
    const prod = GM_getValue("prod", {})
    const met = tblrows[6].innerText.split("\t")
    const krist = tblrows[7].innerText.split("\t")
    const deut = tblrows[8].innerText.split("\t")
    const energy = tblrows[9].innerText.split("\t")
    const met_s = tblrows[20].innerText.split("\t")
    const krist_s = tblrows[21].innerText.split("\t")
    const deut_s = tblrows[22].innerText.split("\t")
    for (let i = 2; i < met.length; i++) {
        met[i] = met[i].replaceAll("/h", "")
        met[i] = met[i].replaceAll(".", "")
        let mt = met[i].split(" ")
        krist[i] = krist[i].replaceAll("/h", "")
        krist[i] = krist[i].replaceAll(".", "")
        let kt = krist[i].split(" ")
        deut[i] = deut[i].replaceAll("/h", "")
        deut[i] = deut[i].replaceAll(".", "")
        let dt = deut[i].split(" ")
        let engry = energy[i].replace(/\./g, "")
        prod[pids[i]] = {met: parseInt(mt[1]), krist: parseInt(kt[1]), deut: parseInt(dt[1])}

        res[pids[i]] = {
            time: now,
            energy: parseInt(engry),
            energy_max: 9999999,
            deut: parseInt(dt[0]),
            deut_max: get_storage_capacity(deut_s[i]),
            krist: parseInt(kt[0]),
            krist_max: get_storage_capacity(krist_s[i]),
            met: parseInt(mt[0]),
            met_max: get_storage_capacity(met_s[i])
        }
    }

    GM_setValue("res", res)
    GM_setValue("prod", prod)

    //gebäude
    const commander_building = GM_getValue("commander_building", {})

    let line_to_building = {
        11: 1,
        12: 2,
        13: 3,
        14: 4,
        16: 12,
        17: 14,
        19: 21,
        20: 22,
        21: 23,
        22: 24,
        23: 31,
        25: 34,
        29: 44
    }
    let line_vals = {}
    for (let line in line_to_building) {
        line_vals[line] = tblrows[line].innerText.split("\t")

    }
    for (let i = 2; i < met.length; i++) {
        let obj = {}
        for (let l in line_to_building) {
            obj[line_to_building[l]] = parseInt(line_vals[l][i])
        }
        commander_building[pids[i]] = obj
    }

    GM_setValue("commander_building", commander_building)

    //flotte

    const commander_fleet = GM_getValue("commander_fleet", {})
    const line_to_fleet = {
        61: 212,
        51: 202,
        52: 203,
        59: 210,
        58: 209,
        57: 208,
        53: 204,
        54: 205,
        55: 206,
        56: 207,
        64: 215,
        60: 211,
        62: 213,
        63: 214
    }
    line_vals = {}
    for (let line in line_to_fleet) {
        line_vals[line] = tblrows[line].innerText.split("\t")

    }

    for (let i = 2; i < met.length; i++) {
        let obj = {}
        for (let l in line_to_fleet) {
            if (line_vals[l].length !== met.length) {
                continue
            }
            obj[line_to_fleet[l]] = parseInt(line_vals[l][i].replace(/\./g, ""))
        }
        commander_fleet[pids[i]] = obj
    }
    GM_setValue("commander_fleet", commander_fleet)

    //def
    const commander_def = GM_getValue("commander_def", {})
    let line_to_def = {
        66: "s401",
        67: "s402",
        68: "s403",
        69: "s404",
        70: "s405",
        72: "s407",
        73: "s408",
        71: "s406",
        75: "s502",
        76: "s503"
    }
    line_vals = {}
    for (let line in line_to_def) {
        line_vals[line] = tblrows[line].innerText.split("\t")

    }
    for (let i = 2; i < met.length; i++) {
        let obj = {}
        for (let l in line_to_def) {
            if (line_vals[l].length !== met.length) {
                continue
            }
            obj[line_to_def[l]] = parseInt(line_vals[l][i].replace(/\./g, ""))
        }
        commander_def[pids[i]] = obj
    }

    GM_setValue("commander_def", commander_def)

}

function get_storage_capacity(lvl) {
    return Math.floor(2.5 * Math.exp(20 * lvl / 33)) * 5000
}

function commander_check_show(id, commander_fleet) {
    let ttx = 0
    for (let ko in commander_fleet) {
        if (!(id in commander_fleet[ko])) {
            commander_fleet[ko][id] = 0
        }
        ttx += parseInt(String(commander_fleet[ko][id]).replaceAll(".", ""))

    }
    return ttx

}

export function commander_building_table() {
    const commander_def = GM_getValue("commander_building", {})
    let html = "   <table>        <tr>          <th>Buildings</th>";
    let shiptypes = {
        "1": "Met",
        "2": "Krist",
        "3": "Deut",
        "4": "Skw",
        "12": "Fkw",
        "14": "Robo",
        "21": "Werft",
        "22": "Met S",
        "23": "Krist S",
        "24": "Deut S",
        "31": "Lab",
        "34": "Idiot - really this is stupid",
        "44": "Silo"
    }
    let showntypes = {};
    let sary = ["1", "2", "3", "4", "12", "22", "23", "24", "14", "21", "31", "44", "34"]
    let showary = []
    for (let k of sary) {
        let amt = commander_check_show(k, commander_def)
        if (amt > 0) {
            showntypes[k] = amt
            showary.push(k)
        }
    }
    let moons = 0
    for (let ko of get_sortet_planet_list(Object.keys(commander_def))) {
        if (ko.includes("M")) {
            moons += 1
        }

    }
    let amtplanets = Object.keys(commander_def).length - moons
    for (let sid of showary) {
        html += `<th>${shiptypes[sid]}</th>`
    }

    html += "</tr><th>Mean</th>"
    for (let sid of showary) {
        html += `<th>${Math.round(10 * showntypes[sid] / amtplanets) / 10}</th>`

    }

    html += "</tr>"
    for (let ko of get_sortet_planet_list(Object.keys(commander_def))) {
        html += `<tr><th>[${ko}</th>`
        for (let sid of showary) {
            if (!(sid in commander_def[ko])) {
                commander_def[ko][sid] = 0
            }
            html += `<td>${commander_def[ko][sid]}</td>`

        }
        html += "</tr>"

    }
    return html + "</table>"
}

export function commander_def_table() {
    const commander_def = GM_getValue("commander_def", {})
    let html = "   <table>        <tr>          <th>Def</th>";
    let shiptypes = {
        "s401": "Rak",
        "s402": "LL",
        "s403": "SL",
        "s404": "Gauß",
        "s405": "Ion",
        "s407": "Ks",
        "s408": "Gs",
        "s406": "Pw",
        "s502": "Ar",
        "s503": "Irak"
    }
    let showntypes = {};
    let sary = ["s401", "s402", "s403", "s405", "s404", "s406", "s502", "s407", "s408"]
    let showary = []
    for (let k of sary) {
        let amt = commander_check_show(k, commander_def)
        if (amt > 0) {
            showntypes[k] = amt
            showary.push(k)
        }
    }
    for (let sid of showary) {
        html += `<th>${shiptypes[sid]}</th>`
    }

    html += "</tr><th>Total</th>"
    for (let sid of showary) {
        html += `<th>${showntypes[sid]}</th>`

    }

    html += "</tr>"
    for (let ko of get_sortet_planet_list(Object.keys(commander_def))) {
        html += `<tr><th>[${ko}</th>`
        for (let sid of showary) {
            if (!(sid in commander_def[ko])) {
                commander_def[ko][sid] = 0
            }
            html += `<td>${commander_def[ko][sid]}</td>`

        }
        html += "</tr>"

    }
    return html + "</table>"
}

export function commander_fleet_table() {
    const commander_fleet = GM_getValue("commander_fleet", {})
    let html = "   <table>        <tr>          <th>Shiptype</th>";
    let shiptypes = {
        212: "Sats",
        202: "KT",
        203: "GT",
        210: "Spio",
        209: "Rec",
        208: "Kolo",
        204: "LJ",
        205: "SJ",
        206: "Xer",
        207: "SS",
        215: "Sxer",
        211: "Bomber",
        213: "Zerr",
        214: "RIP"
    }
    let showntypes = {};
    let sary = [212, 202, 203, 210, 209, 208, 204, 205, 206, 207, 215, 211, 213, 214]
    let showary = []
    for (let k of sary) {
        let amt = commander_check_show(k, commander_fleet)
        if (amt > 0) {
            showntypes[k] = amt
            showary.push(k)
        }
    }
    for (let sid of showary) {
        html += `<th>${shiptypes[sid]}</th>`
    }
    html += "</tr><th>Total</th>"
    for (let sid of showary) {
        html += `<th>${showntypes[sid]}</th>`

    }
    html += "</tr>"

    for (let ko of get_sortet_planet_list(Object.keys(commander_fleet))) {
        html += `<tr><th>[${ko}</th>`
        for (let sid of showary) {
            if (!(sid in commander_fleet[ko])) {
                commander_fleet[ko][sid] = 0
            }
            html += `<td>${commander_fleet[ko][sid]}</td>`

        }
        html += "</tr>"

    }


    return html + "</table>"

}

export function commander_res_table() {
    const res = GM_getValue("res", {})
    const livve_vals = get_current_values()
    let html = "   <table>        <tr>          <th>Res</th>";
    const types = ["met", "krist", "deut", "energy"]
    for (let sid of types) {
        html += `<th>${sid}</th>`
    }
    html += "</tr>"
    for (let ko of get_sortet_planet_list(Object.keys(res))) {
        html += `<tr><th>[${ko}</th>`
        for (let sid of types) {
            if (!(sid in res[ko])) {
                res[ko][sid] = -1
            }

            let maxc = "-1"
            if (typeof livve_vals[ko] == "undefined" || typeof livve_vals[ko][sid] == "undefined" || typeof res[ko] == "undefined" || typeof res[ko][sid + "_max"] == "undefined") {
                html += `<td style='color:gray;font-weight: bolder;'>???</td>`
                break;
            }
            maxc = 120 - Math.round(120 * Math.min(1, livve_vals[ko][sid] / res[ko][sid + "_max"]))

            if (sid === "energy") {
                if (livve_vals[ko][sid] < 0) {
                    maxc = 0
                }
                html += `<td style='color:hsl(${maxc},100%,50%);font-weight: bolder;'>${livve_vals[ko][sid]}</td>`
            } else {
                html += `<td style='color:hsl(${maxc},100%,50%);font-weight: bolder;'>${shortly_number(livve_vals[ko][sid])}</td>`
            }

        }
        html += "</tr>"

    }


    return html + "</table>"

}