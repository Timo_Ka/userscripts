import {GM_getValue, GM_setValue} from "./general";


const sizes = {1: "small", 2: "medium", 3: "big"}


const pirates = {
    1: ["Ein paar anscheinend sehr verzweifelte Weltraumpiraten haben versucht, unsere Expeditionsflotte zu kapern.",
        "Einige primitive Barbaren greifen uns mit Raumschiffen an, die nicht einmal ansatzweise die Bezeichnung Raumschiff verdient haben. Sollte der Beschuss ernst zu nehmende Ausmaße annehmen, sehen wir uns gezwungen das Feuer zu erwidern.",
        "Wir haben ein paar Funksprüche sehr betrunkener Piraten aufgefangen. Anscheinend sollen wir überfallen werden.",
        "Wir mussten uns gegen einige Piraten wehren, die zum Glück nicht allzu zahlreich waren.",
        "Unsere Expeditionsflotte meldet, dass ein gewisser Moa Tikarr und seine wilde Meute die bedingungslose Kapitulation unserer Flotte verlangen. Sollten sie Ernst machen, werden sie feststellen müssen, dass sich unsere Schiffe durchaus zu wehren wissen.",
        'A space pirate in despair tried to commandeer our expeditions fleet.',
        'Some primitive barbarians attacked us with spaceships, at least their ships were in horrible conditions. If the firepower was a serious threat we would have beene forced to reply with our firepower.',
        'We have intercepted a couple of radio messages of some very drunk pirates. We apparently will be attacked.',
        'We had to fight against some pirates who fortunately were outnumbered against us.',
        'Our fleet reports that a certain Moa Tikarr and his crew requested the unconditional surrender of our fleet. If they are serious, they will have to find out that we can defend ourself very well.'
    ],
    2: ["Deine Expeditionsflotte hatte ein unschönes Zusammentreffen mit einigen Weltraumpiraten.",
        "Wir sind in den Hinterhalt einiger Sternen-Freibeuter geraten! Ein Kampf war leider unvermeidlich.",
        "Der Hilferuf, dem die Expedition folgte, stellte sich als böse Falle einiger arglistiger Sternen-Freibeuter heraus. Ein Gefecht war unvermeidlich.",
        'Your expedition fleet had an unforseen encounter with some spacepirates.',
        'We flew into an ambush of some starpirates! Unfortunately a fight was unavoidable.',
        'The call for help of another expeditionfleet turned out to be a trap of some cunning pirates. A battle was unavoidable.'
    ],
    3: ["Die aufgefangenen Signale stammten nicht von Fremdwesen, sondern von einer geheimen Piratenbasis! Die Piraten waren von unserer Anwesenheit in ihrem Sektor nicht besonders begeistert.",
        "Die Expeditionsflotte meldet schwere Kämpfe mit nicht-identifizierten Piratenschiffen!",
        'Wir haben gerade eine dringende Nachricht vom Expeditionskommandanten erhalten: "Sie kommen auf uns zu! Sie sind aus dem Hyperraum gesprungen, zum Glück sind es nur Piraten, wir haben also eine Chance, wir werden kämpfen!"',
        'The intercepted signals did not came from some unknown aliens but of a secret pirate base! The pirates were not very happy by our presence in their sector.',
        'The expedition fleet reported heavy fights with unidentified pirate ships!',
        'We have just received an urgent message from the expedition commander: "They\'re coming at us! They jumped out of hyperspace, fortunately they\'re just pirates, so we have a chance, we\'ll fight!"'
    ]
}
const aliens = {
    1: ['Deine Expeditionsflotte hatte einen nicht besonders freundlichen Erstkontakt mit einer unbekannten Spezies.',
        'Einige fremdartig anmutende Schiffe haben ohne Vorwarnung die Expeditionsflotte angegriffen!',
        'Unsere Expedition wurde von einer kleinen Gruppe unbekannter Schiffe angegriffen!',
        'Die Expeditionsflotte meldet Kontakt mit unbekannten Schiffen. Die Funksprüche sind nicht entschlüsselbar, jedoch scheinen die fremden Schiffe ihre Waffen zu aktivieren.',
        'Your expedition fleet had an uninteresting first contact with new aliens.',
        'Some strange ships have attacked the expedition fleet without early warning!',
        'Our expedition was attacked by a small group of unknown ships!',
        'The expedition fleet reports contact with unknown ships. The radio messages are not able to be decoded, however, the strange ships seem to activate their weapons.'
    ],
    2: ['Eine unbekannte Spezies greift unsere Expedition an!',
        'Deine Expeditionsflotte hat anscheinend das Hoheitsgebiet einer bisher unbekannten, aber äußerst aggressiven und kriegerischen Alienrasse verletzt.',
        'Die Verbindung zu unserer Expeditionsflotte wurde kurzfristig gestört. Soweit wir die letzte Botschaft richtig entschlüsselt haben, steht die Flotte unter schwerem Feuer – die Aggressoren konnten nicht identifiziert werden.',
        'An unknown specie attacks our expedition!',
        'Your expedition fleet has apparently violated the territory of a previously unknown but highly aggressive alien race.',
        'The connection with our expedition fleet was disconnected abruptly. The last what we heard of the fleet was that it was taking heavy fire - the aggressors could not be identified.'],
    3: ['Deine Expedition ist in eine Alien-Invasions-Flotte geraten und meldet schwere Gefechte!',
        'Ein großer Verband kristalliner Schiffe unbekannter Herkunft hält direkten Kollisionskurs mit unserer Expeditionsflotte. Wir müssen nun wohl vom Schlimmsten ausgehen.',
        'Wir hatten ein wenig Schwierigkeiten, den Dialekt der fremden Rasse richtig auszusprechen. Unser Diplomat nannte versehentlich "Feuer!" statt "Frieden!".',
        'Your expedition has come across an alien invasion fleet and reports massive battles!',
        'A large group of crystalline ships of unknown origin is on a direct collision course with our expedition fleet. We must prepare ourself for the worst.',
        'We had a bit of difficulty pronouncing the dialect of the alien race correctly. Our diplomat accidentally called']
}

const res_found = {
    1: ['Deine Expedition hat einen kleinen Asteroidenschwarm entdeckt, aus dem einige Ressourcen gewonnen werden können.',
        'Auf einem abgelegenen Planetoiden wurden einige leicht zugängliche Ressourcenfelder gefunden und erfolgreich Rohstoffe gewonnen.',
        'Deine Expedition stieß auf sehr alte Raumschiffwracks einer längst vergangenen Schlacht. Einzelne Komponenten konnte man bergen und recyceln.',
        'Die Expedition stieß auf einen radioaktiv verstrahlten Planetoiden mit hochgiftiger Atmosphäre. Jedoch ergaben Scans, dass dieser Planetoid sehr rohstoffhaltig ist. Mittels automatischer Drohnen wurde versucht, ein Maximum an Rohstoffen zu gewinnen.',
        'Your expedition has discovered a small asteroids cluster from which some resources could be won.',
        'Some easily accessible resource fields were found and raw materials were won successfully on the remote planetoid.',
        'The expedition discovered a planet contaminated by radiation with a highly-poisonous atmosphere. Scans, however, shown that this planet contains alot of raw material. It was mined by automatic drones to win a maximum of raw materials.'
    ],
    2: ['Deine Expedition fand einen uralten, voll beladenen, aber menschenleeren Frachterkonvoi. Einige Ressourcen konnten geborgen werden.',
        'Auf einem kleinen Mond mit eigener Atmosphäre fand deine Expedition große Rohstoffvorkommen. Die Bodencrews sind dabei diese natürlichen Schätze zu heben.',
        'Wir haben einen kleinen Konvoi ziviler Schiffe getroffen, die dringend Nahrung und Medikamente benötigten. Im Austausch dafür erhielten wir eine ganze Menge nützlicher Ressourcen.',
        'Your expedition found an ancient freighter convoy which is fully loaded but deserted. Some resources could be yielded.',
        'Your expedition found great raw material deposits on a little moon with an atmosphere of it`s own. The ships crew is going to yield this treasure.',
        'We have met a small convoy of civilian ships which urgently needed food and medicine. In the exchange for it we got a whole set of useful resources.'],
    3: ['Deine Expeditionsflotte meldet den Fund eines riesigen Alien-Schiffswracks. Mit der Technologie konnten sie zwar nichts anfangen, aber das Schiff ließ sich in seine Einzelteile zerlegen und dadurch konnte man wertvolle Rohstoffe gewinnen.',
        'Ein Mineraliengürtel um einen unbekannten Planeten enthielt Unmengen an Rohstoffen. Die Expeditionsflotte meldet volle Lager!',
        'Your expedition fleet reported the find of a gigantic alien shipwreck. But the ships were of no purpose because its technology, however the ship could be chopped into pieces for its components so the crew could win valuable raw materials.',
        'The asteroid belt around an unknown planet contained vast amounts of raw materials. The expedition fleet filled its storage rooms!']
}

const ship_found = {
    1: ['Wir sind auf die Überreste einer Vorgängerexpedition gestoßen! Unsere Techniker schauen, ob sie einige der Wracks wieder Flugfähig bekommen',
        'Wir haben eine verlassene Piratenbasis gefunden. Im Hangar liegen noch einige alte Schiffe. Unsere Techniker schauen nach, ob einige davon noch zu gebrauchen sind.',
        'Unsere Expedition fand einen Planeten, der wohl durch anhaltende Kriege fast komplett zerstört wurde. In der Umlaufbahn trieben diverse Schiffswracks. Die Techniker versuchen, einige davon zu reparieren. Vielleicht erhalten wir so auch Information darüber, was hier geschehen ist.',
        'Deine Expedition ist auf eine alte Sternenfestung gestoßen, die wohl seit Ewigkeiten verlassen ist. Im Hangar der Festung wurden ein paar Schiffe gefunden. Die Techniker schauen, ob sie einige davon wieder flott bekommen.',
        'We have discovered the remains of a predecessor expedition! Our technicians look, whether they get some of the wrecks working again.',
        'We have found a deserted pirate base. Some old ships are still located in the hangar. Our technicians look, whether still some of the ships are useable.',
        'Our expedition found a planet which was almost completly destroyed by wars. Various shipwrecks drifted in the orbit. The technicians try to repair some of these. Perhaps we also find information on what has happened here.',
        'The expedition stumbled upon an old starbase which seems to be abandoned for an eternity. A couple of ships were found in the hangar of the fortress. The technicians managed to get some of them flying again.'],
    2: ['Wir haben die Reste einer Armada gefunden. Die Techniker der Expeditionsflotte haben sich sofort auf die halbwegs intakten Schiffe begeben und versuchen, diese wieder instandzusetzen.',
        'Unsere Expedition stieß auf eine alte, automatische Schiffswerft. Einige Schiffe sind noch in der Produktionsphase, und unsere Techniker versuchen, die Energieversorgung der Werft wiederherzustellen.',
        'We have found the remains of an armada. The technicians of the expedition have immediately gone onto the partly intact ships and were able to repair some of them.',
        'Our expedition discovered an old, automatic shipyard. Some ships still were in the production phase and our technicians tried to finish the construction, the energy supply of the shipyard first needed to be restored.'],
    3: ['Wir haben einen riesigen Raumschiffsfriedhof gefunden. Einigen Technikern der Expeditionsflotte ist es gelungen, das eine oder andere Schiff wieder in Betrieb zu nehmen.',
        'Wir haben einen Planeten mit Resten einer Zivilisation entdeckt. Aus dem Orbit ist noch ein riesiger Raumbahnhof zu erkennen, der als einziges Gebäude noch intakt ist. Einige unserer Techniker und Piloten haben sich auf die Oberfläche begeben um nachzuschauen, ob ein paar der dort abgestellten Schiffe noch zu gebrauchen sind.',
        'We have found a gigantic ship cemetery. Some technicians of the expedition have managed to get the ship into operation again.',
        'We have discovered a planet with remains of a civilization. Another gigantic ship station which is still intact has been spotted from outerspace. Some of our technicians and pilots have gone onto the surface to look, whether a couple of the ships can be retrieved.']
}

const return_speed = {
    1: ['Eine unvorhergesehene Rückkopplung in den Energiespulen der Antriebsaggregate beschleunigte den Rücksprung der Expedition, so dass sie nun früher als erwartet zurückkehrt. Ersten Meldungen zufolge hat sie jedoch nichts Spannendes zu berichten.',
        'Der etwas wagemutige neue Kommandant nutzte ein instabiles Wurmloch, um den Rückflug zu verkürzen – mit Erfolg! Jedoch hat die Expedition selbst keine neuen Erkenntnisse gebracht.',
        'Deine Expedition meldet keinen Besonderheiten in dem erforschten Sektor. Jedoch geriet die Flotte beim Rücksprung in einen Sonnenwind. Dadurch wurde der Sprung ziemlich beschleunigt. Deine Expedition kehrt nun etwas früher nach Hause.',
        'An unforeseen relay in the energy coils of the drive aggregates accelerated the return of the expedition. According to first reports it has however, nothing thrilling to report.',
        'A new daring commanding officer used an unstable wormhole as a shortcut to return home, with succes! The expedition, however, has not brought new knowledge itself.',
        'No unusual features in the investigated sector have been found on your expedition. The fleet, however, got into a solar wind at the return flight. Because of this your expedition returned a bit earlier as expected.'],
    2: [
        'Ein böser Patzer des Navigators führte zu einer Fehlkalkulation beim Sprung der Expedition. Nicht nur landete die Flotte an einem völlig falschen Ort, auch der Rückweg nahm nun erheblich mehr Zeit in Anspruch.',
        'Aus bisher unbekannten Gründen, ging der Sprung der Expeditionsflotte völlig daneben. Beinahe wäre man im Herzen einer Sonne herausgekommen. Zum Glück ist man in einem bekannten System gelandet, jedoch wird der Rücksprung länger dauern als ursprünglich gedacht.',
        'Das neue Navigationsmodul hat wohl doch noch mit einigen Bugs zu kämpfen. Nicht nur ging der Sprung der Expeditionsflotte in die völlig falsche Richtung, auch wurde das gesamte Deuterium verbraucht, wobei der Sprung der Flotte nur knapp hinter dem Mond des Startplaneten endete. Etwas enttäuscht kehrt die Expedition nun auf Impuls zurück. Dadurch wird die Rückkehr wohl ein wenig verzögert.',
        'Deine Expedition geriet in einen Sektor mit verstärkten Partikelstürmen. Dadurch überluden sich die Energiespeicher der Flotte und bei sämtlichen Schiffen fielen die Hauptsysteme aus. Deine Mechaniker konnten das Schlimmste verhindern, jedoch wird die Expedition nun mit einiger Verspätung zurückkehren.',
        'Das Führungsschiff deiner Expeditionsflotte kollidierte mit einem fremden Schiff, das ohne Vorwarnung direkt in die Flotte sprang. Das fremde Schiff explodierte und die Schäden am Führungsschiff waren beachtlich. Sobald die gröbsten Reparaturen abgeschlossen sind, werden sich deine Schiffe auf den Rückweg machen, da in diesem Zustand die Expedition nicht fortgeführt werden kann.',
        'Der Sternwind eines roten Riesen verfälschte den Sprung der Expedition dermaßen, dass es einige Zeit dauerte, den Rücksprung zu berechnen. Davon abgesehen gab es in dem Sektor, in dem die Expedition herauskam, nichts außer der Leere zwischen den Sternen.',
        'A miscalculation of the navigator at the expedition landed the fleet at a completely wrong place, the way back took substantially more time now.',
        'For some unknown reasons the jump of the expedition fleet missed it`s target completely. The fleet almost would have come out in the heart of a sun. Fortunately it landed in a known system, the return will, however, last longer than thought originally.',
        'The new navigation module still has a few bugs in it. The expedition fleet did not only go in the completely wrong direction, the complete deuterium was also consumed, because of this the return is delayed a little.',
        'Your expedition got into a sector with amplified particle storms. The energy storages of the fleet overloaded themselves because of this and for all ships the main systems were cancelled. Your mechanics could prevent the worst but the expedition will return with some delay now.',
        'The commandship of your expedition fleet collided with a strange ship which jumped directly in front of the fleet without early warning. The strange ship exploded and the damages to the leadership ship were considerable. Wanting to as soon as the roughest repairs are completed make your ships to themselves on the way back, since in this condition the expedition cannot be continued.',
        'The star wind of a red giant distorted the jump of the expedition so that the ships navigationsystem calculated the reentry incorrect. As a result the expedition return somewhere in the emptiness between the stars and the destination.',
    ]
}

const black_hole = {
    1: ['Von der Expedition ist nur noch folgender Funkspruch übrig geblieben: Zzzrrt Oh Gott! Krrrzzzzt das zrrrtrzt sieht krgzzzz ja aus wie Krzzzzzzzztzzzz...',
        'Das Letzte, was von dieser Expedition noch gesendet wurde, waren einige unglaublich gut gelungene Nahaufnahmen eines sich öffnenden, schwarzen Loches.',
        'Ein Kernbruch des Führungsschiffes führte zu einer Kettenreaktion, die in einer durchaus spektakulären Explosion die gesamte Expedition vernichtete.',
        'Die Expeditionsflotte ist nicht mehr aus dem Sprung in den Normalraum zurückgekehrt. Unsere Wissenschaftler rätseln noch immer, was geschehen sein könnte, jedoch scheint die Flotte endgültig verloren zu sein.',
        'The only thing left of the expedition is the following radio message: Zzzrrt oh God!...Krrrzzzzt the zrrrtrzt sees krgzzzz yes from how Krzzzzzzzztzzzz...',
        'The last thing we received of the expedition were some unbelievable good close-ups of an opening black hole.',
        'A nuclear breach on one of the commanding ships led to a chain reaction which destroyed the complete expedition in a spectacular explosion.',
        'The expedition fleet has not returned from the hyperspacejump. Our scientists are still puzzled on what could have happened, the fleet however seems to be lost definitely.']
}


function add_to_db(key, nr, ships) {
    const expodb = GM_getValue("expo_counts", {
        "res": [0, 0, 0],
        "pirates": [0, 0, 0],
        "aliens": [0, 0, 0],
        "speed": [0, 0],
        "hole": [0],
        "ships": [0, 0, 0],
        "nothing": [0]
    })
    const ship_res_db = GM_getValue("expo_ship_res", {"res": [0, 0, 0], "fleet": {}})
    expodb[key][nr - 1] += 1
    if (key === "res" && ships != null) {
        expodb[key][nr - 1] -= 1
        ship_res_db["res"][0] += ships[0]
        ship_res_db["res"][1] += ships[1]
        ship_res_db["res"][2] += ships[2]
    }
    if (key === "ships") {
        for (let stype in ships) {
            if (!(stype in ship_res_db["fleet"])) {
                ship_res_db["fleet"][stype] = 0
            }
            ship_res_db["fleet"][stype] += ships[stype]
        }
    }
    GM_setValue("expo_counts", expodb)
    GM_setValue("expo_ship_res", ship_res_db)
}

function check_att(text, atype) {
    for (let ptype in atype) {
        for (let txt of atype[ptype]) {
            if (text.includes(txt)) {
                return ptype
            }
        }
    }
    return 0
}

function get_ships(text) {
    let ships = {}
    for (let sp of text) {
        if (sp.match(/^.+: \d+/g)) {
            let shipname = sp.split(":")[0].trim()
            ships[shipname] = parseInt(sp.split(":")[1].trim().replace(/\./g, ""))
        }

    }
    return ships
}

function get_res_found(text) {
    let res = [0, 0, 0]
    for (let sp of text) {
        let rgmtc = sp.trim().match(/(\d+\.?)+.+\./g)
        if (rgmtc == null) {
            continue
        }
        if (rgmtc[0].includes("href")) {
            continue
        }
        if (rgmtc) {
            let idx = 0
            if (sp.includes("ium")) {
                idx = 2
            }
            if (sp.includes("sta")) {
                idx = 1
            }
            res[idx] = parseInt(rgmtc[0].replaceAll(".", "").match(/\d+/g))
        }
    }
    return res
}


export function selectmesages() {
    const expo_ids = GM_getValue("expo_handled_ids", {})
    const mesages = document.getElementsByClassName("message_head")
    for (let msg of mesages) {
        if (msg.childNodes[5].innerText === "Flottenhauptquatier" || msg.childNodes[5].innerText === "Fleet Headquarters") {
            let expo = msg.childNodes[7].innerText.includes("xpedit")
            let text = document.getElementsByClassName("messages_body " + msg.id)[0].innerHTML.split("<br>");
            const idv = msg.id
            if (expo) { //checking the events
                let itext = document.getElementsByClassName("messages_body " + msg.id)[0].innerText;

                let resf = check_att(itext, res_found)
                if (resf !== 0) {
                    if (expo_ids[idv] !== 1) {
                        expo_ids[idv] = 1
                        let res = get_res_found(text)
                        add_to_db("res", 1, res)
                        GM_setValue("expo_handled_ids", expo_ids)
                        add_to_db("res", resf)
                    }

                    document.getElementsByClassName("messages_body " + msg.id)[0].children[0].innerHTML += "<br> <b style='color:#90EE90'>res: " + sizes[resf] + "</b>"
                    continue
                }

                let shipf = check_att(itext, ship_found)
                if (shipf !== 0) {
                    let ships = get_ships(text)
                    if (expo_ids[idv] !== 1) {
                        expo_ids[idv] = 1
                        GM_setValue("expo_handled_ids", expo_ids)
                        add_to_db("ships", shipf, ships)
                    }
                    document.getElementsByClassName("messages_body " + msg.id)[0].children[0].innerHTML += "<br> <b style='color:#90EE90'>ships: " + sizes[shipf] + "</b>"
                    continue
                }
                let pirate = check_att(itext, pirates)
                if (pirate !== 0) {
                    if (expo_ids[idv] !== 1) {
                        expo_ids[idv] = 1
                        GM_setValue("expo_handled_ids", expo_ids)
                        add_to_db("pirates", pirate)
                    }

                    document.getElementsByClassName("messages_body " + msg.id)[0].children[0].innerHTML += "<br> <b style='color:#FFFF00'>pirates: " + sizes[pirate] + "</b>"
                    continue
                }
                let alien = check_att(itext, aliens)
                if (alien !== 0) {
                    if (expo_ids[idv] !== 1) {
                        expo_ids[idv] = 1
                        GM_setValue("expo_handled_ids", expo_ids)
                        add_to_db("aliens", alien)
                    }

                    document.getElementsByClassName("messages_body " + msg.id)[0].children[0].innerHTML += "<br> <b style='color:#FF0000'>aliens: " + sizes[alien] + "</b>"
                    continue
                }

                let speed = check_att(itext, return_speed)
                if (speed !== 0) {
                    if (expo_ids[idv] !== 1) {
                        expo_ids[idv] = 1
                        GM_setValue("expo_handled_ids", expo_ids)
                        add_to_db("speed", speed)
                    }

                    document.getElementsByClassName("messages_body " + msg.id)[0].children[0].innerHTML += "<br> <b style='color:#FFFF00'>speed: " + {
                        1: "fast",
                        2: "slow"
                    }[speed] + "</b>"
                    continue
                }
                let bh = check_att(itext, black_hole)
                if (bh !== 0) {
                    if (expo_ids[idv] !== 1) {
                        expo_ids[idv] = 1
                        GM_setValue("expo_handled_ids", expo_ids)
                        add_to_db("hole", 1)
                    }

                    document.getElementsByClassName("messages_body " + msg.id)[0].children[0].innerHTML += "<br> <b style='color:#FF4500'>big oof</b>"
                    continue
                }
                if (expo_ids[idv] !== 1) {
                    expo_ids[idv] = 1
                    GM_setValue("expo_handled_ids", expo_ids)
                    add_to_db("nothing", 1)
                }

                document.getElementsByClassName("messages_body " + msg.id)[0].children[0].innerHTML += "<br> <b style='color:#FFFFFF'>nothing</b>"

            }

        }
    }
}


function gen_obj(ttx, key) {
    const robj = {ttl: 0, size: ttx[key]}
    for (let v of ttx[key]) {
        robj.ttl += v
    }
    return robj
}

export function expo_table() {
    const expodb = GM_getValue("expo_counts", {
        "res": [0, 0, 0],
        "pirates": [0, 0, 0],
        "aliens": [0, 0, 0],
        "speed": [0, 0],
        "hole": [0],
        "ships": [0, 0, 0],
        "nothing": [0]
    })
    const ship_res_db = GM_getValue("expo_ship_res", {"res": [0, 0, 0], "fleet": {}})


    const res = gen_obj(expodb, "res")
    res.met = ship_res_db.res[0]
    res.krist = ship_res_db.res[1]
    res.deut = ship_res_db.res[2]
    const pirate = gen_obj(expodb, "pirates")
    const alien = gen_obj(expodb, "aliens")
    const ships = gen_obj(expodb, "ships")
    const speed = gen_obj(expodb, "speed")
    const hole = gen_obj(expodb, "hole")
    const nothing = gen_obj(expodb, "nothing")
    let ttl = nothing.ttl + hole.ttl + speed.ttl + ships.ttl + alien.ttl + pirate.ttl + res.ttl
    let txtr = `
           <table>
        <tr>
          <th>Event</th>
          <th>Total ${ttl}</th>
          <th>Percent</th>
        </tr>
          <tr>
          <td>Res<br>s:${res.size[0]} <br>m:${res.size[1]}<br>b:${res.size[2]}</td>
          <td>${res.ttl}<br>met:${shortly_number(res.met)}<br>krist:${shortly_number(res.krist)}<br>deut:${shortly_number(res.deut)}</td>
          <td>${Math.round(res.ttl * 1000 / ttl) / 10}<br>met:${shortly_number(res.met / (res.ttl + 0.00001))}<br>krist:${shortly_number(res.krist / (res.ttl + 0.00001))}<br>deut:${shortly_number(res.deut / (res.ttl + 0.00001))}</td>
        </tr>
         <tr>
          <td>Ships <br>s:${ships.size[0]} <br>m:${ships.size[1]}<br>b:${ships.size[2]}</td>
          <td>${ships.ttl}</td>
          <td>${Math.round(ships.ttl * 1000 / ttl) / 10}</td>
        </tr>

        <tr>
          <td>Pirates <br>s:${pirate.size[0]} <br>m:${pirate.size[1]}<br>b:${pirate.size[2]}</td>
          <td>${pirate.ttl}</td>
          <td>${Math.round(pirate.ttl * 1000 / ttl) / 10}</td>
        </tr>
        <tr>
          <td>Aliens <br>s:${alien.size[0]} <br>m:${alien.size[1]}<br>b:${alien.size[2]}</td>
          <td>${alien.ttl}</td>
          <td>${Math.round(alien.ttl * 1000 / ttl) / 10}</td>
        </tr>
        <tr>
          <td>speed <br>fast:${speed.size[0]} <br>slow:${speed.size[1]}</td>
          <td>${speed.ttl}</td>
          <td>${Math.round(speed.ttl * 1000 / ttl) / 10}</td>
        </tr>
        <tr>
          <td>nixname</td>
          <td>${hole.ttl}</td>
          <td>${Math.round(hole.ttl * 1000 / ttl) / 10}</td>
        </tr>
          <td>nichts</td>
          <td>${nothing.ttl}</td>
          <td>${Math.round(nothing.ttl * 1000 / ttl) / 10}</td>
        </tr>

      </table>
    `

    let expo_ships = ship_res_db.fleet
    let stable = `
                  <table>
        <tr>
          <th>shiptype</th>
          <th>Amount</th>
        </tr>`
    let snames = ["Spionagesonde", "Kleiner Transporter", "Großer Transporter", "Leichter Jäger", "Schwerer Jäger", "Kreuzer", "Schlachtschiff", "Schlachtkreuzer", "Bomber", "Zerstörer", "Spy Probe", "Light Cargo", "Heavy Cargo", "Light Fighter", "Heavy Fighter", "Cruiser", "Battleship", "Battle Cruiser", "Planet Bomber", "Star Fighter"]
    for (let stype of snames) {
        if (!(stype in expo_ships)) {
            continue
        }
        stable += `
                        <tr>
          <td>${stype}</td>
          <td>${expo_ships[stype]}</td>
        </tr>
            `
    }
    stable += "    </table>"

    return txtr + stable

}
