import {get_current_planet_id, GM_getValue, GM_setValue} from "./general";

export function update_current_res() {
    const planet_res = GM_getValue("res", {})
    let coords = get_current_planet_id()

    planet_res[coords] = {
        met: parseInt(document.getElementById("current_metal").innerText.replace(/\./g, "")),
        krist: parseInt(document.getElementById("current_crystal").innerText.replace(/\./g, "")),
        deut: parseInt(document.getElementById("current_deuterium").innerText.replace(/\./g, "")),
        met_max: parseInt(document.getElementById("max_metal").innerText.replace(/\./g, "")),
        krist_max: parseInt(document.getElementById("max_crystal").innerText.replace(/\./g, "")),
        deut_max: parseInt(document.getElementById("max_deuterium").innerText.replace(/\./g, "")),
        energy: parseInt(("" + document.getElementById("resources_mobile").childNodes[7].innerText.split("/")[0].split("\n")[1]).replaceAll(".", "")),
        energy_max: 99999999,
        time: Date.now()
    }
    GM_setValue("res", planet_res)
}

export function calctime() {

    let met_needed = Math.max(Number(document.getElementById("t_met").value) - Number(document.getElementById("c_met").innerText), 0)
    let krist_needed = Math.max(Number(document.getElementById("t_krist").value) - Number(document.getElementById("c_krist").innerText), 0)
    let deut_needed = Math.max(Number(document.getElementById("t_deut").value) - Number(document.getElementById("c_deut").innerText), 0)

    let met_time = 100 * met_needed / Number(document.getElementById("p_met").innerText)
    let krist_time = 100 * krist_needed / Number(document.getElementById("p_krist").innerText)
    let deut_time = 100 * deut_needed / Number(document.getElementById("p_deut").innerText)
    document.getElementById("time_til_got").innerText = Math.round(Math.max(met_time, krist_time, deut_time)) / 100 + " h";
}

export function set_planets() {
    const planets_selected = GM_getValue("select", {})
    let planets = document.getElementById("planetSelector")
    for (let p of planets.children) {
        let pname = p.innerText.split("[")[1]
        if (p.innerText.includes("(")) {
            pname = pname + "M"
        }
        if (!(pname in planets_selected)) {
            planets_selected[pname] = 0;
        }
    }
    GM_setValue("select", planets_selected)
}


export function show_prodph() {
    const planet_production = GM_getValue("prod", {})
    let met = 0
    let krist = 0
    let deut = 0
    let met_t = 0
    let krist_t = 0
    let deut_t = 0
    const planet_res = GM_getValue("res", {})
    for (let planet in planet_production) {
        if (!document.getElementById(planet).checked) {
            continue
        }

        met += planet_production[planet].met
        krist += planet_production[planet].krist
        deut += planet_production[planet].deut

    }
    for (let planet in planet_res) {
        if (!document.getElementById(planet).checked) {
            continue
        }
        met_t += planet_res[planet].met
        krist_t += planet_res[planet].krist
        deut_t += planet_res[planet].deut

    }

    document.getElementById("p_met").innerText = met;
    document.getElementById("p_krist").innerText = krist;
    document.getElementById("p_deut").innerText = deut;

    document.getElementById("met_top_p").innerHTML = "+" + shortly_number(met);
    document.getElementById("krist_top_p").innerHTML = "+" + shortly_number(krist);
    document.getElementById("deut_top_p").innerHTML = "+" + shortly_number(deut);


    document.getElementById("c_met").innerText = met_t;
    document.getElementById("c_krist").innerText = krist_t;
    document.getElementById("c_deut").innerText = deut_t;


}


export function update_production() {
    if (document.getElementsByTagName("form").length == 1) {
        if (document.getElementsByTagName("form")[0].action.includes("page=resources")) {
            let f1 = document.getElementsByTagName("form")[0]
            let resperhour = f1.getElementsByTagName("tr")[f1.getElementsByTagName("tr").length - 3].innerText.replace(/\./g, "").split("\t")
            let coords = get_current_planet_id()
            const planet_production = GM_getValue("prod", {})
            planet_production[coords] = {
                met: parseInt(resperhour[1]),
                krist: parseInt(resperhour[2]),
                deut: parseInt(resperhour[3])
            }
            GM_setValue("prod", planet_production)

        }
    }
}

export function saveres() {
    GM_setValue("res_input", [document.getElementById("t_met").value, document.getElementById("t_krist").value, document.getElementById("t_deut").value])
    calctime();
}

export function get_current_values() {
    const planet_production = GM_getValue("prod", {})
    const planet_res = GM_getValue("res", {})

    let ctime = Date.now()
    const live_perp = {}
    for (let p in planet_production) {
        live_perp[p] = {"met": -1, "krist": -1, "deut": -1, energy: planet_res[p].energy}
        const hours = (ctime - planet_res[p].time) / 3600000;
        const res_current = {}
        const fulltimes = {}
        for (let res of ["met", "krist", "deut"]) {
            let prod = hours * planet_production[p][res]
            let lres = planet_res[p][res]
            let current_res = Math.floor(Math.max(lres, Math.min(planet_res[p][res + "_max"], lres + prod)))

            let fulltime = Math.floor(Math.max(0, (planet_res[p][res + "_max"] - current_res) / planet_production[p][res]))
            res_current[res] = current_res
            fulltimes[res] = fulltime
            live_perp[p][res] = current_res

        }
        let minfulltime = Math.min(...Object.values(fulltimes))
        let min_res = "gold"
        for (let res in fulltimes) {
            if (minfulltime == fulltimes[res]) {
                min_res = res
            }

        }
        live_perp[p].ftime = minfulltime
        live_perp[p].fres = min_res

    }
    return live_perp

}

export function calcfull() {

    const planets_selected = GM_getValue("select", {})
    const live_res = {met: 0, krist: 0, deut: 0}
    const planet_live = get_current_values()
    for (let p in planet_live) {
        for (let res of ["met", "krist", "deut"]) {
            if (planets_selected[p] == 1) {
                live_res[res] += planet_live[p][res]
            }
        }
        document.getElementById("full_" + p).innerText = "  " + planet_live[p].ftime + " h " + planet_live[p].fres;
    }
    document.getElementById("c_met").innerText = live_res.met
    document.getElementById("c_krist").innerText = live_res.krist
    document.getElementById("c_deut").innerText = live_res.deut
    document.getElementById("met_top").innerHTML = shortly_number(live_res.met)
    document.getElementById("krist_top").innerHTML = shortly_number(live_res.krist)
    document.getElementById("deut_top").innerHTML = shortly_number(live_res.deut)
}