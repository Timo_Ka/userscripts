import {GM_getValue, GM_setValue} from "./general";

export function custom_fleet_table() {


    return `
<table  id="customfleet">
<tr>
<th colspan=3><select id="cfleet_select" style="width:100%">

</select></th>
</tr>
<tr><th colspan=3><input id="cfleet_name" placeholder="name" style="width:90%"></th>
</tr>
<tr>
<td colspan=1 >Expo points:</td>
<td colspan=2 id="ship_expo_points"></td>
</tr>


<tr>
          <th>Shiptype</th>
          <th colspan=2>Amount </th>
        </tr>
        <tr>
          <td>KT</td>
          <td colspan=2><input id="ship_202" type="number" value="0"></td>
        </tr>
         <tr>
          <td>GT</td>
          <td colspan=2><input id="ship_203" type="number" value="0"></td>
        </tr>
        <tr>
          <td>LJ</td>
          <td colspan=2><input id="ship_204" type="number" value="0"></td>
        </tr>
        <tr>
          <td>SJ</td>
          <td colspan=2><input id="ship_205" type="number" value="0"></td>
        </tr>
         <tr>
          <td>Xer</td>
          <td colspan=2><input id="ship_206" type="number" value="0"></td>
        </tr>
                <tr>
          <td>SS</td>
          <td colspan=2><input id="ship_207" type="number" value="0"></td>
        </tr>
                <tr>
          <td>Sxer</td>
          <td colspan=2><input id="ship_215" type="number" value="0"></td>
        </tr>
         <tr>
          <td>Zerr</td>
          <td colspan=2><input id="ship_213" type="number" value="0"></td>
        </tr>
         <tr>
          <td>Bomber</td>
          <td colspan=2><input id="ship_211" type="number" value="0"></td>
        </tr>
        <tr>
                    <td>RIP</td>
          <td colspan=2><input id="ship_214" type="number" value="0"></td>
        </tr>

         <tr>
              <td>Spio</td>
          <td colspan=2><input id="ship_210" type="number" value="0"></td>
        </tr>
         <tr>
              <td>Recycler</td>
          <td colspan=2><input id="ship_209" type="number" value="0"></td>
        </tr>
        <tr>
              <td>Kolo</td>
          <td colspan=2><input id="ship_208" type="number" value="0"></td>
        </tr>

   <tr>
              <td colspan=3><center><button id="cf_save" style="width:25%">
              Save
              </button><button  id="cf_del" style="width:25%">
              Remove
              </button></center></td>
        </tr>



</table>
        `


}

function save_fleet_Select() {
    let obj = {}
    let cfleet = GM_getValue("custom_fleets", {})
    for (let shipid of ["202",
        "203",
        "204",
        "205",
        "206",
        "207",
        "210",
        "211",
        "213",
        "215",
        "214",
        "209",
        "208"
    ]) {
        obj[shipid] = document.getElementById("ship_" + shipid).value
    }
    let fname = document.getElementById("cfleet_name").value
    if (!(fname in cfleet)) {
        let obj = document.createElement("option");
        obj.value = fname;
        obj.innerText = fname;
        document.getElementById("cfleet_select").appendChild(obj);
    }

    cfleet[fname] = obj
    GM_setValue("custom_fleets", cfleet)
}

function change_fleet_select() {

    showfleet(document.getElementById("cfleet_select").value);
}

function cf_remove() {
    let cfleet = GM_getValue("custom_fleets", {})
    let oname = document.getElementById("cfleet_name").value
    let rv = confirm("Remove fleet template " + oname + " ?")

    if (rv === false) {
        return;
    }

    document.getElementById("cfleet_name").value = "";
    let rmv = document.querySelector("#cfleet_select option[value='" + oname + "']")
    if (rmv) {
        rmv.remove()
        delete cfleet[oname];
        showfleet(document.getElementById("cfleet_select").value)
    }

    GM_setValue("custom_fleets", cfleet)
}

function showfleet(name) {
    let cfleet = GM_getValue("custom_fleets", {})
    document.getElementById("cfleet_name").value = name;
    for (let sid in cfleet[name]) {
        document.getElementById("ship_" + sid).value = cfleet[name][sid]

    }
}

function showexpopoints() {
    let fleet = {}
    for (let f in exp_values) {

        fleet[f] = document.getElementById("ship_" + f).value

    }

    document.getElementById("ship_expo_points").innerText = calcexpopoints(fleet)


}


export function calcexpopoints(fleet) {
    let points = 0;
    for (let f in fleet) {
        if (exp_values[f] === null) {
            continue;
        }
        points += exp_values[f] * parseInt(fleet[f])

    }
    return points;


}

export function loadcustomfleets() {
    let cfleet = GM_getValue("custom_fleets", {})
    for (let cf in cfleet) {
        showfleet(cf)
        break
    }
    for (let cf in cfleet) {
        let obj = document.createElement("option");
        obj.value = cf;
        obj.innerText = cf;
        document.getElementById("cfleet_select").appendChild(obj);
    }

    document.getElementById("cfleet_select").addEventListener("change", change_fleet_select);
    document.getElementById("cf_save").addEventListener("click", save_fleet_Select);
    document.getElementById("cf_del").addEventListener("click", cf_remove);
    for (let f in exp_values) {
        document.getElementById("ship_" + f).addEventListener("change", showexpopoints);

    }
    showexpopoints();

}



const exp_values = {
    "202": 20,
    "203": 60,
    "204": 20,
    "205": 50,
    "206": 135,
    "207": 300,
    "208": 150,
    "209": 80,
    "210": 5,
    "211": 375,
    "213": 550,
    "214": 45000,
    "215": 350
}