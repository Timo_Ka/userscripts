# gimmecharts


## Installation

To develop using the webpack framework you need to have nodejs installed.
You need to install packages on 2 layers

1. this directory `npm install` installs all packages that are required by your userscript itself, defined in `package.json`
2. the main directory of this repository, `cd ../.. & npm install` to install the required tools e.g. webpack

## Live Reload / Development mode

For development you can install a proxy userscript that automatically loads the latest version of your userscript from the filesystem.
This only works on Chrome, see [setup instructions](https://www.tampermonkey.net/faq.php#Q204) to grant Chrome / TM access to the local filesystem.

To get your script into local hotreload development mode, simply do

```sh
cd src/gimmecharts
npm run serve # will start webpack with watchmode to build on filechange and open local development server on port :8080
# go to http://localhost:8080 and install the gimmecharts.proxy.user.js script in tampermonkey
```

## Delivery

The version of the script is defined in the `webpack.config.js` within the `tamperMonkeyHeader` variable.

To build a production version of the script run

```sh
npm run build
```

the output will be placed into `dist/` folder.
From there copy it to the root directory of this repo.
