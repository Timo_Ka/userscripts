// ==UserScript==
// @name        warsim
// @version     0.5.0
// @author      joghurtrucksack
// @description automatically set non-battle units to 0 when simulating
// @include     /https:\/\/(www.|)pr0game\.com\/game\.php\?page=battleSimulator/
// @downloadURL https://codeberg.org/pr0game/userscripts/raw/branch/master/warsim.user.js
// @updateURL   https://codeberg.org/pr0game/userscripts/raw/branch/master/warsim.user.js
// ==/UserScript==

(()=>{"use strict";const t=[202,203,208,209,210,214],e={};function n(t,n=0){const l=document.getElementsByName(`battleinput[0][${n}][${t}]`);if(1===l.length){const n=l[0];e[t]=n.value}}function l(t,e=0){const n=document.getElementsByName(`battleinput[0][${e}][${t}]`);1===n.length&&(n[0].value=0)}function o(t,n=!0){const l=document.getElementsByName(`battleinput[0][0][${t}]`);if(1===l.length){const o=l[0],u=e[t],c=document.evaluate("../../td",o,null,XPathResult.FIRST_ORDERED_NODE_TYPE,null).singleNodeValue;c.addEventListener("click",(()=>{o.value=u})),n&&(c.style.cursor="pointer",c.title="Click to reset to MAX")}}!function(){for(let t=202;t<=215;t++)n(t),o(t);for(const e of t)l(e)}()})();